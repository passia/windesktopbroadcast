# WinDesktopBroadcast

## Purpose

Use WinDesktopBroadcast to stream your Windows desktop over UDP.

## Usage

```
Arguments: [-crf N] --host host --video-port port --audio-port port

  --host host
        This is the host name or IP address where the RTP stream will be broadcasted.

  --port port
        This is the port number where the RTP video stream will be broadcasted.

Other arguments:

  --frame-rate <frame rate>
        Set the output frame rate. Use low frame rate if the frame encoding takes too much time.
        Default frame rate is 20.

  --crf 0-51
        This is the Constant Rate Factor used as quality setting for the H264 encoder.

        0: lossless but the bit rate is HUGE
        23: quite good quality but the bit rate may be too big in some cases
        30 (default value): the quality is correct but the bit rate is reasonable
        51: worst quality possible but with extra small bit rate.

Examples:

  Simple example: WinDesktopBroadcast.exe --host 192.168.0.5 --port 1234
  Example with CRF: WinDesktopBroadcast.exe -crf 28 --host 192.168.0.5 --port 1234
```

## Compatibility

Developed and tested on Windows 10 x64

## FAQ

### How can I see my desktop "remotely" ?

Considering the port of wich you started WinDesktopBroadcast is `1234`.
With VLC, open a network stream `udp://@:1234`.

### I see a lot of "Frame encoding takes too much time !" in the console logs, what does it mean ?

It means that your computer cannot encode the video stream on-the-fly and skips somes frames. This causes some lag in your video stream.
To increase the encoding speed, you can:

  * reduce your screen resolution to 1280x720 pixels, which is the video stream resolution.
  * lower the stream frame rate with the `--frame-rate` program argument.
  * pass `--crf` argument to WinDesktopBroadcast with a higher value.

## Remarks / Bugs

Drop me an email at aurelien D0T passion 4T gmail D0T com
