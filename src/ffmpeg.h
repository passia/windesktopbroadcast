/**
 * WinDesktopBroadcast - free Windows desktop streaming program.
 *
 * Copyright (C) 2016  Aur�lien Passion
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __FFMPEG_H__
#define __FFMPEG_H__

#include "stdafx.h"

/**
 * Initialize the FFmpeg context.
 *
 * @param videoRtpUrl the video RTP destination URL
 * @param captureWidth the frame width in the input stream
 * @param captureHeight the frame height in the input stream
 * @param width the frame width in the output stream
 * @param height the frame height in the output stream
 * @param frameRate the output stream frame rate
 * @param crf the H264 Constant Rate Factor
 * @param audioRtpUrl the audio RTP destination URL
 * @param sampleRate the audio sample rate
 * @param numChannels the number of audio channels
 * @param bytesPerSample the number of bytes per audio sample
 */
bool ffmpeg_init(const char *videoRtpUrl,
    int captureWidth,
    int captureHeight,
    int width,
    int height,
    int frameRate,
    const char *crf,
	int sampleRate,
	uint16_t numChannels,
	uint16_t bytesPerSample);

/**
 * Release the allocated FFmpeg resources.
 *
 * @return <i>true</i> if everything went OK
 */
void ffmpeg_release();

/**
 * Add a video frame to the stream.
 *
 * @param bits the frame data
 * @param height the height of the frame
 * @param lineSize the size of one line of the frame
 *
 * @return <i>true</i> if everything went OK
 */
bool ffmpeg_add_video_frame(void *bits, int height, int lineSize);

/**
 * Add an audio frame to the audio stream.
 *
 * @param bits the audio data. Sample are signed 16-bit packed in 2 channels.
 * @param numSamples the number of audio samples
 *
 * @return <i>true</i> if everything went OK
 */
bool ffmpeg_add_audio_frame(int16_t *bits, unsigned short numSamples);

#endif // __FFMPEG_H__
