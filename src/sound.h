#ifndef __SOUND_H__
#define __SOUND_H__

#include "stdafx.h"

/**
 * Initialize the sound system.
 *
 * @param[out] pAudioBytesPerSample the number of bytes per audio sample
 * @param[out] pSampleRate the sample rate
 * @param[out] pNumChannels the number of channels
 * @param[out] pBufferFrameCount the number of frames in the buffer
 *
 * @return <i>true</i> if everything went OK
 */
bool sound_init(unsigned short *pAudioBytesPerSample, unsigned int *pSampleRate, unsigned short *pNumChannels, unsigned int *pBufferFrameCount);

/**
 * Get an audio frame.
 * Since maybe not all available audio data is returned, you should call this function while ''pDone'' is false.
 *
 * @param[out] pData a pointer to an array to store the audio data
 * @param[out] pNumFramesAvailable the number of frames available in the data
 * @param[out] pDone if the capture is done
 * @param[out] pSilence if the captured audio was silence
 *
 * @return <i>true</i> if everything went OK
 */
bool sound_get_frame(int16_t **pData, uint32_t *pNumFramesAvailable, bool *pDone, bool *pSilence);

/**
 * Release the soud system resources.
 *
 * @return <i>true</i> if everything went OK
 */
bool sound_release();

#endif //__SOUND_H__
