/**
 * WinDesktopBroadcast - free Windows desktop streaming program.
 *
 * Copyright (C) 2016  Aur�lien Passion
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stdafx.h"

#include "direct3d.h"
#include "ffmpeg.h"
#include "sound.h"

#define STREAM_DEFAULT_FRAME_RATE       20
#define STREAM_X264_DEFAULT_CRF         "30"

#define DST_WIDTH   1280
#define DST_HEIGHT  720

char host[64] = "";
char crf[16] = STREAM_X264_DEFAULT_CRF;
uint32_t frameRate = STREAM_DEFAULT_FRAME_RATE;
char output[128];
char videoPort[16] = "";
char audioOutput[128];
char videoOutput[128];

bool stopMainLoop;

void print_usage() {
    std::cout << "Arguments: [-crf N] --host host --video-port port"
#ifndef NO_AUDIO
        << " --audio-port port"
#endif // NO_AUDIO
        << std::endl
        << std::endl;
    std::cout << "--host host" << std::endl;
    std::cout << "        This is the host name or IP address where the RTP stream will be broadcasted." << std::endl
        << std::endl;
    std::cout << "--port port" << std::endl;
    std::cout << "        This is the port number where the RTP video stream will be broadcasted." << std::endl
        << std::endl;
    std::cout << "Other arguments:" << std::endl
        << std::endl;
    std::cout << "  --frame-rate <frame rate>" << std::endl;
    std::cout << "        Set the output frame rate. Use low frame rate if the frame encoding takes too much time." << std::endl;
    std::cout << "        Default frame rate is 20." << std::endl
        << std::endl;
    std::cout << "  --crf 0-51" << std::endl;
    std::cout << "        This is the Constant Rate Factor used as quality setting for the H264 encoder." << std::endl
        << std::endl;
    std::cout << "        0: lossless but the bit rate is HUGE" << std::endl;
    std::cout << "        23: quite good quality but the bit rate may be too big in some cases" << std::endl;
    std::cout << "        30 (default value): the quality is correct but the bit rate is reasonable" << std::endl;
    std::cout << "        51: worst quality possible but with extra small bit rate." << std::endl
        << std::endl;
    std::cout << "Examples:" << std::endl
        << std::endl;
    std::cout << "  Simple example: WinDesktopBroadcast.exe --host 192.168.0.5 --port 1234"
        << std::endl;

    std::cout << "  Example with CRF: WinDesktopBroadcast.exe -crf 28 --host 192.168.0.5 --port 1234"
        << std::endl;
}

void parse_args(int argc, char **argv)
{
    int i;
    for (i = 1; i < argc; ++i) {
        std::string param = std::string(argv[i]);
        if (param == "--host") {
            if (++i >= argc) break;
            strncpy_s(host, argv[i], sizeof(host));
        }
        else if (param == "--port") {
            if (++i >= argc) break;
            strncpy_s(videoPort, argv[i], sizeof(videoPort));
        }
        else if (param == "--crf") {
            if (++i >= argc) break;
            strncpy_s(crf, argv[i], sizeof(crf));
        }
        else if (param == "--frame-rate") {
            if (++i >= argc) break;
            frameRate = atoi(argv[i]);
        }
        else if (param == "--help" || param == "-h") {
            print_usage();
            exit(0);
        }
        else {
            break;
        }
    }

    // Bad usage
    if (strnlen_s(host, sizeof(host)) == 0
        || strnlen_s(videoPort, sizeof(videoPort)) == 0)
    {
        print_usage();
        exit(1);
    }

    std::cout << "Frame rate:" << frameRate << std::endl;
    std::cout << "CRF: " << crf << std::endl;

    sprintf_s(videoOutput, "udp://%s:%s", host, videoPort);
    std::cout << "Output: " << videoOutput << std::endl;
}

BOOL WINAPI ConsoleCtrlHandler(DWORD fdwCtrlType)
{
    switch (fdwCtrlType)
    {
    case CTRL_C_EVENT:
        stopMainLoop = true;
        return TRUE;

    default:
        return FALSE;
    }
}

int main(int argc, char **argv)
{
    parse_args(argc, argv);

    ULONGLONG start;
    ULONGLONG now;
    ULONGLONG frameStart;
    float fpsSinceStart = -1;
    void *bits;
    int lineSize;

#ifndef NO_AUDIO
    int16_t *pAudioData;
    uint32_t numAudioFramesAvailable;
    uint16_t audioBytesPerSample;
    uint32_t audioSampleRate;
    uint16_t numChannels;
    uint32_t bufferFrameCount;
#endif //NO_AUDIO

    int captureWidth, captureHeight;

    if (!d3d_init(&captureWidth, &captureHeight))
    {
        std::cerr << "Error initializing Direct 3D" << std::endl;
        exit(1);
    }

#ifndef NO_AUDIO
    if (!sound_init(&audioBytesPerSample, &audioSampleRate, &numChannels, &bufferFrameCount))
    {
        std::cerr << "Error initializing sound system" << std::endl;
        exit(1);
    }

    pAudioData = new int16_t[bufferFrameCount];
#endif //NO_AUDIO

#ifndef NO_AUDIO
    if (!ffmpeg_init(videoOutput, captureWidth, captureHeight, DST_WIDTH, DST_HEIGHT, frameRate, crf,
        audioSampleRate, numChannels, audioBytesPerSample))
    {
        std::cerr << "Error initializing FFmpeg" << std::endl;
        exit(1);
    }
#else
    if (!ffmpeg_init(videoOutput, captureWidth, captureHeight, DST_WIDTH, DST_HEIGHT, frameRate, crf,
        0, 0, 0))
    {
        std::cerr << "Error initializing FFmpeg" << std::endl;
        exit(1);
    }
#endif //NO_AUDIO

    SetConsoleCtrlHandler(ConsoleCtrlHandler, TRUE);

    ULONGLONG timeBetweenFramesMs = 1000 / frameRate;

    start = GetTickCount64();
    now = start;

    std::cout << "Starting encoding loop..." << std::endl;
    stopMainLoop = false;
    while (!stopMainLoop)
    {
        std::cout << ".";
        //std::cout << now - startTime << " s" << std::endl;

        frameStart = GetTickCount64();

        d3d_get_frame(&bits, &lineSize);

#ifndef NO_AUDIO
        bool audioDone = false;
        bool silence = false;
        while (!audioDone) {
            sound_get_frame(&pAudioData, &numAudioFramesAvailable, &audioDone, &silence);

            if (silence) {
                std::cout << "O";
            }
            else if (!audioDone) {
                ffmpeg_add_audio_frame(pAudioData, numAudioFramesAvailable);
            }
        }
#endif //NO_AUDIO

        ffmpeg_add_video_frame(bits, captureHeight, lineSize);

        d3d_release_frame();

        now = GetTickCount64();
        ULONGLONG timeToEncode = now - frameStart;
        if (timeToEncode < timeBetweenFramesMs) {
            DWORD delay = (DWORD)(timeBetweenFramesMs - timeToEncode);
            Sleep(delay);
        }
        else {
            std::cerr << "Frame encoding takes too much time !" << std::endl;
        }
    }

    ffmpeg_release();
    d3d_release();
#ifndef NO_AUDIO
    delete[] pAudioData;
    sound_release();
#endif //NO_AUDIO

    return 0;
}
