/**
 * WinDesktopBroadcast - free Windows desktop streaming program.
 *
 * Copyright (C) 2016  Aur�lien Passion
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DIRECT3D_H__
#define __DIRECT3D_H__

/**
 * Initialize a Direct 3D 9 context (device and surface).
 */
bool d3d_init(int *captureWidth, int *captureHeight);

/**
 * Release the Direct 3D context.
 */
void d3d_release();

/**
 * Grab a frame from the Direct 3D context.
 *
 * @param pBits on success, the frame data should be here
 * @param pLineSize on success, the size of a line of the frame should be stored here
 * @return true on success
 */
bool d3d_get_frame(void **pBits, int *pLineSize);

/**
 * Release the resources allocated for the last call to 'd3d_get_frame'.
 */
bool d3d_release_frame();

#endif //__DIRECT3D_H__
