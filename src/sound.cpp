
#include "stdafx.h"

#include <Functiondiscoverykeys_devpkey.h>

#define SAFE_RELEASE(punk)  \
              if ((punk) != NULL)  \
                { (punk)->Release(); (punk) = NULL; }

const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);
const IID IID_IAudioClient = __uuidof(IAudioClient);
const IID IID_IAudioCaptureClient = __uuidof(IAudioCaptureClient);

IMMDeviceEnumerator *pEnumerator = NULL;
IMMDevice *pDevice = NULL;
IAudioClient *pAudioClient = NULL;
IAudioCaptureClient *pCaptureClient = NULL;
WAVEFORMATEX *pwfx = NULL;

bool sound_init(unsigned short *pAudioBytesPerSample, unsigned int *pSampleRate, unsigned short *pNumChannels, unsigned int *pBufferFrameCount)
{
    HRESULT hr;
    REFERENCE_TIME hnsRequestedDuration = 10000000;
    UINT32 nBlockAlign;
    IPropertyStore *pProperties;

    CoInitialize(NULL);

    // create audio device enumerator
    hr = CoCreateInstance(
        CLSID_MMDeviceEnumerator,
        NULL, CLSCTX_ALL,
        IID_IMMDeviceEnumerator,
        (void**)&pEnumerator);
    if (FAILED(hr)) {
        std::cerr << "Error creating audio device enumerator" << std::endl;
        return false;
    }

    // get default render device
    hr = pEnumerator->GetDefaultAudioEndpoint(
        eRender, eConsole, &pDevice);
    if (FAILED(hr)) {
        std::cerr << "Error getting default audio rendering device" << std::endl;
        return false;
    }

    // get device properties
    hr = pDevice->OpenPropertyStore(STGM_READ, &pProperties);
    if (FAILED(hr)) {
        std::cerr << "Error opening audio device properties" << std::endl;
        return false;
    }

    // print device name
    PROPVARIANT pv;
    PropVariantInit(&pv);
    pProperties->GetValue(PKEY_Device_FriendlyName, &pv);
    if (FAILED(hr)) {
        std::cerr << "Error getting audio device name" << std::endl;
        return false;
    }
    std::wcout << "Audio device to use: " << pv.pwszVal << std::endl;

    // activate an audio client
    hr = pDevice->Activate(
        IID_IAudioClient, CLSCTX_ALL,
        NULL, (void**)&pAudioClient);
    if (FAILED(hr)) {
        std::cerr << "Error activating audio client" << std::endl;
        return false;
    }

    // get mix format
    hr = pAudioClient->GetMixFormat(&pwfx);
    if (FAILED(hr)) {
        std::cerr << "Error getting audio mix format" << std::endl;
        return false;
    }
    nBlockAlign = pwfx->nBlockAlign;

    // force sample format to signed 16 bits
    if (pwfx->wFormatTag == WAVE_FORMAT_EXTENSIBLE) {
        PWAVEFORMATEXTENSIBLE pwfEx = reinterpret_cast<PWAVEFORMATEXTENSIBLE>(pwfx);
        if (IsEqualGUID(KSDATAFORMAT_SUBTYPE_IEEE_FLOAT, pwfEx->SubFormat)) {
            pwfEx->SubFormat = KSDATAFORMAT_SUBTYPE_PCM;
            pwfEx->Samples.wValidBitsPerSample = 16;
            pwfx->wBitsPerSample = 16;
            pwfx->nBlockAlign = pwfx->nChannels * pwfx->wBitsPerSample / 8;
            pwfx->nAvgBytesPerSec = pwfx->nBlockAlign * pwfx->nSamplesPerSec;
        }
        else {
            std::cerr << "Audio format subtype is not handled" << std::endl;
            return false;
        }
    }
    else {
        std::cerr << "Audio capture format not handled: " << pwfx->wFormatTag << std::endl;
        return false;
    }

    // initialize the audio client
    hr = pAudioClient->Initialize(
        AUDCLNT_SHAREMODE_SHARED,
        AUDCLNT_STREAMFLAGS_LOOPBACK,
        hnsRequestedDuration,
        0,
        pwfx,
        NULL);
    if (FAILED(hr)) {
        std::cerr << "Error initializing the audio client" << std::endl;
        return false;
    }

    // Get the size of the allocated buffer.
    hr = pAudioClient->GetBufferSize(pBufferFrameCount);
    if (FAILED(hr)) {
        std::cerr << "Error getting audio client buffer size" << std::endl;
        return false;
    }

    // Get an audio capture client
    hr = pAudioClient->GetService(
        IID_IAudioCaptureClient,
        (void**)&pCaptureClient);
    if (FAILED(hr)) {
        std::cerr << "Error getting audio capture client / hr = " << hr << std::endl;
        return false;
    }

    // Start recording
    hr = pAudioClient->Start();
    if (FAILED(hr)) {
        std::cerr << "Error starting audio client" << std::endl;
        return false;
    }

    // copy output parameters
    *pAudioBytesPerSample = pwfx->wBitsPerSample / 8;
    *pSampleRate = pwfx->nSamplesPerSec;
    *pNumChannels = pwfx->nChannels;

    return true;
}

bool sound_get_frame(int16_t **pData, uint32_t *pNumFramesAvailable, bool *pDone, bool *pSilence)
{
    HRESULT hr;
    BYTE *pBufferData;
    DWORD flags;
    UINT32 packetLength = 0;

    hr = pCaptureClient->GetNextPacketSize(&packetLength);
    if (FAILED(hr)) {
        std::cerr << "Error getting next audio packet size" << std::endl;
        return false;
    }

    *pDone = packetLength == 0;

    hr = pCaptureClient->GetBuffer(
        &pBufferData,
        pNumFramesAvailable,
        &flags, NULL, NULL);
    if (FAILED(hr)) {
        std::cerr << "Error getting audio buffer" << std::endl;
        return false;
    }

    *pSilence = (flags & AUDCLNT_BUFFERFLAGS_SILENT) != 0;
    if (!*pSilence) {
        memcpy(*pData, pBufferData, *pNumFramesAvailable * pwfx->nChannels * pwfx->wBitsPerSample / 8);
    }

    hr = pCaptureClient->ReleaseBuffer(*pNumFramesAvailable);
    if (FAILED(hr)) {
        std::cerr << "Error releasing audio buffer" << std::endl;
        return false;
    }

    return true;
}

bool sound_release()
{
    SAFE_RELEASE(pEnumerator);
    SAFE_RELEASE(pDevice);
    SAFE_RELEASE(pAudioClient);
    SAFE_RELEASE(pCaptureClient);

    return true;
}
