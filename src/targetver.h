/**
 * WinDesktopBroadcast - free Windows desktop streaming program.
 *
 * Copyright (C) 2016  Aur�lien Passion
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

// Si vous incluez SDKDDKVer.h, cela d�finit la derni�re plateforme Windows disponible.

// Si vous souhaitez g�n�rer votre application pour une plateforme Windows pr�c�dente, incluez WinSDKVer.h et
// d�finissez la macro _WIN32_WINNT � la plateforme que vous souhaitez prendre en charge avant d'inclure SDKDDKVer.h.

#include <SDKDDKVer.h>
