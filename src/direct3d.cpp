/**
 * WinDesktopBroadcast - free Windows desktop streaming program.
 *
 * Copyright (C) 2016  Aur�lien Passion
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "stdafx.h"

IDirect3DDevice9 *pD3DDevice;
IDirect3DSurface9 *pD3DSurface;
D3DLOCKED_RECT lr;

bool d3d_init(int *captureWidth, int *captureHeight)
{
    int ret;

    IDirect3D9 *pDirect3D = Direct3DCreate9(D3D_SDK_VERSION);
    D3DPRESENT_PARAMETERS PresentParams;
    memset(&PresentParams, 0, sizeof(D3DPRESENT_PARAMETERS));
    PresentParams.Windowed = TRUE;
    PresentParams.SwapEffect = D3DSWAPEFFECT_DISCARD;

    ret = pDirect3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, GetDesktopWindow(),
        D3DCREATE_SOFTWARE_VERTEXPROCESSING, &PresentParams, &pD3DDevice);

    if (ret != D3D_OK) {
        std::cerr << "Error creating D3D device" << std::endl;
        return false;
    }

    // get the desktop device context
    HDC hdc = GetDC(GetDesktopWindow());

    // get the height and width of the screen
    *captureWidth = GetDeviceCaps(hdc, HORZRES);
    *captureHeight = GetDeviceCaps(hdc, VERTRES);
    std::cout << "Resolution:" << *captureWidth << "x" << *captureHeight << std::endl;

    ret = pD3DDevice->CreateOffscreenPlainSurface(*captureWidth, *captureHeight,
        D3DFMT_A8R8G8B8, D3DPOOL_SCRATCH, &pD3DSurface, NULL);

    if (ret != D3D_OK) {
        std::cerr << "Error creating offscreen plain surface" << std::endl;
        return false;
    }

    return true;
}

void d3d_release()
{
    // release Direct 3d surface
    pD3DSurface->Release();
    pD3DSurface = NULL;

    // release Direct 3D device
    pD3DDevice->Release();
    pD3DDevice = NULL;
}

bool d3d_get_frame(void **pBits, int *pLineSize)
{
    int ret;

    pD3DDevice->GetFrontBufferData(0, pD3DSurface);

    ret = pD3DSurface->LockRect(&lr, NULL, D3DLOCK_READONLY);
    if (ret != D3D_OK) {
        std::cerr << "Cannot lock Direct 3D surface" << std::endl;
        return false;
    }

    *pBits = lr.pBits;
    *pLineSize = lr.Pitch;

    return true;
}

bool d3d_release_frame()
{
    int ret;

    ret = pD3DSurface->UnlockRect();
    if (ret != D3D_OK) {
        std::cerr << "Cannot unlock Direct 3D surface" << std::endl;
        return false;
    }

    return true;
}
