/**
 * WinDesktopBroadcast - free Windows desktop streaming program.
 *
 * Copyright (C) 2016  Aur�lien Passion
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ffmpeg.h"
#include "stdafx.h"

#define STREAM_PIX_FMT        AV_PIX_FMT_YUV420P
#define STREAM_GOP_SIZE       12
#define STREAM_X264_PRESET    "ultrafast"
#define STREAM_X264_TUNE      "zerolatency"

#define AUDIO_INPUT_SAMPLE_FMT  AV_SAMPLE_FMT_S16
#define AUDIO_OUTPUT_SAMPLE_FMT  AV_SAMPLE_FMT_S16P

AVFormatContext *pOutputContext;

AVOutputFormat *pVideoOutputFormat;

AVCodecContext *pAudioCodecContext;
AVCodecContext *pVideoCodecContext;

AVStream *pAudioStream;
AVStream *pVideoStream;

AVFrame *pSrcAudioFrame;
AVFrame *pEncAudioFrame;
AVFrame *pSrcVideoFrame;
AVFrame *pEncVideoFrame;

// Resample context
SwrContext *pSwrCtx;

// Rescale context
SwsContext *pSwsCtx;

int64_t nextAudioPts;
int frameCount;

uint16_t audioSampleRate;
uint16_t audioNumChannels;
uint16_t audioBytesPerSample;

/**
* Add a video (or audio) stream with the given codec ID.
*/
AVStream * add_video_stream(AVCodecID codecId, AVCodec **codec, int width, int height, int frameRate, const char *crf)
{
    AVStream *stream;

    // find the encoder
    *codec = avcodec_find_encoder(codecId);
    if (!(*codec)) {
        fprintf(stderr, "Could not find encoder for '%s'\n",
            avcodec_get_name(codecId));
        exit(1);
    }

    // add a new stream
    stream = avformat_new_stream(pOutputContext, NULL);
    if (!stream) {
        fprintf(stderr, "Could not allocate stream\n");
        exit(1);
    }
    stream->id = pOutputContext->nb_streams - 1;

    // allocate codec context
    pVideoCodecContext = avcodec_alloc_context3(*codec);
    if (!pVideoCodecContext) {
        std::cerr << "Error allocating video codec context" << std::endl;
        return NULL;
    }

    switch ((*codec)->type) {
    case AVMEDIA_TYPE_VIDEO:
        pVideoCodecContext->codec_id = codecId;
        pVideoCodecContext->width = width;
        pVideoCodecContext->height = height;
        pVideoCodecContext->time_base.den = frameRate;
        pVideoCodecContext->time_base.num = 1;
        pVideoCodecContext->pix_fmt = STREAM_PIX_FMT;
        pVideoCodecContext->gop_size = STREAM_GOP_SIZE;

        av_opt_set(pVideoCodecContext->priv_data, "allow_skip_frames", "1", 0);
        av_opt_set(pVideoCodecContext->priv_data, "crf", crf, 0);
        av_opt_set(pVideoCodecContext->priv_data, "preset", STREAM_X264_PRESET, 0);
        av_opt_set(pVideoCodecContext->priv_data, "tune", STREAM_X264_TUNE, 0);

        break;

    default:
        break;
    }

    // some formats want stream headers to be separate.
    if (pOutputContext->oformat->flags & AVFMT_GLOBALHEADER)
        pVideoCodecContext->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    // copy stream codec parameters from allocated codec context
    avcodec_parameters_from_context(stream->codecpar, pVideoCodecContext);

    return stream;
}

AVStream * add_audio_stream(AVCodecID codecId, unsigned short numChannels, AVCodec **codec)
{
    int ret;
    char errorBuff[128];
    AVStream *stream;

    // find the encoder
    *codec = avcodec_find_encoder(codecId);
    if (!(*codec)) {
        fprintf(stderr, "Could not find encoder for '%s'\n",
            avcodec_get_name(codecId));
        exit(1);
    }

    // add a new stream
    stream = avformat_new_stream(pOutputContext, NULL);
    if (!stream) {
        fprintf(stderr, "Could not allocate stream\n");
        exit(1);
    }
    stream->id = pOutputContext->nb_streams - 1;

    // allocate codec context
    pAudioCodecContext = avcodec_alloc_context3(*codec);
    if (!pAudioCodecContext) {
        std::cerr << "Error allocating audio codec context" << std::endl;
        return NULL;
    }

    switch ((*codec)->type) {
    case AVMEDIA_TYPE_AUDIO:
        pAudioCodecContext->codec_id = codecId;
        pAudioCodecContext->sample_rate = 44100;
        pAudioCodecContext->channels = numChannels;
        pAudioCodecContext->channel_layout = AV_CH_LAYOUT_STEREO;
        pAudioCodecContext->sample_fmt = AUDIO_OUTPUT_SAMPLE_FMT;
        break;

    default:
        break;
    }

    // some formats want stream headers to be separate.
    if (pOutputContext->oformat->flags & AVFMT_GLOBALHEADER)
        pAudioCodecContext->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    // copy stream codec parameters from allocated codec context
    ret = avcodec_parameters_from_context(stream->codecpar, pAudioCodecContext);
    if (ret < 0) {
        std::cerr << "Could not get audio codec parameters: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
        return NULL;
    }

    return stream;
}

bool open_video(AVCodec *codec, int captureWidth, int captureHeight)
{
    int ret;

    // open the codec
    ret = avcodec_open2(pVideoCodecContext, codec, NULL);
    if (ret < 0) {
        std::cerr << "Could not open video codec" << std::endl;
        return false;
    }

    // allocate the source frame (to store the Direct 3D surface content)
    pSrcVideoFrame = av_frame_alloc();
    pSrcVideoFrame->format = AV_PIX_FMT_BGRA;
    pSrcVideoFrame->width = captureWidth;
    pSrcVideoFrame->height = captureHeight;
    av_frame_get_buffer(pSrcVideoFrame, 64);

    // allocate the encoding frame
    pEncVideoFrame = av_frame_alloc();
    pEncVideoFrame->format = pVideoCodecContext->pix_fmt;
    pEncVideoFrame->width = pVideoCodecContext->width;
    pEncVideoFrame->height = pVideoCodecContext->height;
    av_frame_get_buffer(pEncVideoFrame, 64);

    frameCount = 0;

    return true;
}

bool open_audio(AVCodec *codec)
{
    int ret;
    char errorBuff[128];

    // open the codec
    ret = avcodec_open2(pAudioCodecContext, codec, NULL);
    if (ret < 0) {
        std::cerr << "Could not open audio codec" << std::endl;
        std::cerr << "Could not get audio codec parameters: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
        return false;
    }

    nextAudioPts = 0;

    return true;
}

/**
* Initialize FFmpeg.
*/
bool ffmpeg_init(const char *videoUrl,
    int captureWidth,
    int captureHeight,
    int width,
    int height,
    int frameRate,
    const char *crf,
    int sampleRate,
    uint16_t numChannels,
    uint16_t bytesPerSample)
{
    AVCodec *video_codec = NULL;
    AVCodec *audio_codec = NULL;
    int ret;
    char errorBuff[128];

    pOutputContext = NULL;

    pVideoOutputFormat = NULL;

    pVideoStream = NULL;

    audioSampleRate = sampleRate;
    audioNumChannels = numChannels;
    audioBytesPerSample = bytesPerSample;

    // Initialize libavcodec, and register all codecs and formats.
    avformat_network_init();

    // allocate the video output media context
    ret = avformat_alloc_output_context2(&pOutputContext, NULL, "mpegts", videoUrl);
    if (ret < 0) {
        std::cerr << "Could not initialize FFmpeg output format: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
    }

    if (!pOutputContext) {
        std::cerr << "Could not deduce video output format from URL" << std::endl;
        return false;
    }

    // create video output format
    pVideoOutputFormat = pOutputContext->oformat;
    if (!pVideoOutputFormat) {
        std::cerr << "Error creating video out format" << std::endl;
        return false;
    }

    pVideoOutputFormat->video_codec = AV_CODEC_ID_H264;

    std::cout << "Video codec:" << avcodec_get_name(pVideoOutputFormat->video_codec) << std::endl;
    if (pVideoOutputFormat->video_codec == AV_CODEC_ID_NONE) {
        std::cerr << "Video codec not found" << std::endl;
        return false;
    }

#ifndef NO_AUDIO

    //pVideoOutputFormat->audio_codec = AV_CODEC_ID_VORBIS;
    pVideoOutputFormat->audio_codec = AV_CODEC_ID_MP3;

    std::cout << "Audio codec:" << avcodec_get_name(pVideoOutputFormat->audio_codec) << std::endl;
    if (pVideoOutputFormat->audio_codec == AV_CODEC_ID_NONE) {
        std::cerr << "Audio codec not found" << std::endl;
        return false;
    }
#endif //NO_AUDIO

    // add video stream
    pVideoStream = add_video_stream(pVideoOutputFormat->video_codec, &video_codec, width, height, frameRate, crf);
    if (pVideoStream == NULL) {
        std::cerr << "Error adding video stream" << std::endl;
        return false;
    }

    // initialize the rescale/convert context
    pSwsCtx = sws_getContext(captureWidth, captureHeight, AV_PIX_FMT_BGRA,
        pVideoCodecContext->width, pVideoCodecContext->height, (AVPixelFormat)pVideoCodecContext->pix_fmt,
        SWS_BILINEAR, NULL, NULL, NULL);
    if (!pSwsCtx) {
        std::cerr << "Could not initialize the video conversion context" << std::endl;
        return false;
    }

#ifndef NO_AUDIO
    // add audio stream
    pAudioStream = add_audio_stream(pVideoOutputFormat->audio_codec, numChannels, &audio_codec);
    if (pAudioStream == NULL) {
        std::cerr << "Error adding audio stream" << std::endl;
        return false;
    }

    if (!open_audio(audio_codec)) {
        return false;
    }

    // initialize the resample context
    pSwrCtx = swr_alloc_set_opts(NULL,
        pAudioCodecContext->channel_layout,
        pAudioCodecContext->sample_fmt,
        pAudioCodecContext->sample_rate,
        AV_CH_LAYOUT_STEREO,
        AUDIO_INPUT_SAMPLE_FMT,
        audioSampleRate,
        0, NULL);
    if (!pSwsCtx) {
        std::cerr << "Could not allocate the audio conversion context" << std::endl;
        return false;
    }

    ret = swr_init(pSwrCtx);
    if (ret < 0) {
        std::cerr << "Could not initialize the audio conversion context: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
        return false;
    }
#endif //NO_AUDIO

    open_video(video_codec, captureWidth, captureHeight);
    av_dump_format(pOutputContext, 0, videoUrl, 1);

    // open video stream
    if (!(pVideoOutputFormat->flags & AVFMT_NOFILE)) {
        ret = avio_open(&pOutputContext->pb, videoUrl, AVIO_FLAG_WRITE);
        if (ret < 0) {
            std::cerr << "FFmpeg error: AVFMT_NOFILE: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
            return false;
        }
    }

    // write video header
    ret = avformat_write_header(pOutputContext, NULL);
    if (ret < 0) {
        std::cerr << "Error occurred when writing audio stream header: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
        return false;
    }

    return true;
}

void ffmpeg_release()
{
    // write the trailer to the video and audio streams
    av_write_trailer(pOutputContext);

    // close the codec contexts
#ifndef NO_AUDIO
    avcodec_close(pAudioCodecContext);
#endif //NO_AUDIO
    avcodec_close(pVideoCodecContext);

    // close the outputs
    if (!(pVideoOutputFormat->flags & AVFMT_NOFILE)) {
        avio_close(pOutputContext->pb);
    }

    // free the allocated frames
    av_frame_free(&pSrcVideoFrame);
    av_frame_free(&pEncVideoFrame);
}

bool ffmpeg_add_video_frame(void *bits, int captureHeight, int lineSize)
{
    int ret;
    char errorBuff[128];

    ++frameCount;
    //pSrcFrame->data[0] = (uint8_t*)bits; // no copy
    memcpy(pSrcVideoFrame->data[0], bits, captureHeight * lineSize); // copy

    pSrcVideoFrame->linesize[0] = lineSize;

    AVPacket pkt = { 0 };
    av_init_packet(&pkt);

    sws_scale(pSwsCtx,
        pSrcVideoFrame->data, pSrcVideoFrame->linesize,
        0, captureHeight,
        pEncVideoFrame->data, pEncVideoFrame->linesize);

    // encode the frame
    pEncVideoFrame->pts = frameCount;

    ret = avcodec_send_frame(pVideoCodecContext, pEncVideoFrame);
    if (ret < 0) {
        std::cerr << "Error sending frame: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
        return false;
    }

    ret = avcodec_receive_packet(pVideoCodecContext, &pkt);
    if (ret < 0) {
        if (ret == AVERROR(EAGAIN)) {
            // no data available
            return true;
        }
        std::cerr << "Error receiving packet: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
        return false;
    }

    pkt.stream_index = pVideoStream->index;
    av_packet_rescale_ts(&pkt, pVideoCodecContext->time_base, pVideoStream->time_base);

    ret = av_write_frame(pOutputContext, &pkt);
    if (ret < 0) {
        std::cerr << "Error while writing video frame" << std::endl;
        return false;
    }

    return true;
}

bool ffmpeg_add_audio_frame(int16_t *bits, unsigned short numSamples)
{
    int ret;
    char errorBuff[128];

    // allocate the audio source frame (to store the captured audio content)
    AVFrame *pSrcAudioFrame = av_frame_alloc();
    pSrcAudioFrame->format = AUDIO_INPUT_SAMPLE_FMT;
    pSrcAudioFrame->channels = audioNumChannels;
    pSrcAudioFrame->channel_layout = AV_CH_LAYOUT_STEREO;
    pSrcAudioFrame->sample_rate = audioSampleRate;
    pSrcAudioFrame->nb_samples = numSamples;
    av_frame_get_buffer(pSrcAudioFrame, 32);

    // allocate the audio encoding frame
    AVFrame *pEncAudioFrame = av_frame_alloc();
    pEncAudioFrame->format = pAudioCodecContext->sample_fmt;
    pEncAudioFrame->channels = pAudioCodecContext->channels;
    pEncAudioFrame->channel_layout = pAudioCodecContext->channel_layout;
    pEncAudioFrame->sample_rate = pAudioCodecContext->sample_rate;
    pEncAudioFrame->nb_samples = pAudioCodecContext->frame_size;
    av_frame_get_buffer(pEncAudioFrame, 32);

    // copy by packets of pAudioCodecContext->frame_size max
    int nbSentSamples = 0;
    while (nbSentSamples < numSamples)
    {
        AVPacket pkt = { 0 };
        int nbSamplesToSend = pAudioCodecContext->frame_size;
        if (nbSentSamples + nbSamplesToSend > numSamples)
        {
            nbSamplesToSend = numSamples - nbSentSamples;
        }

        // copy input data into source frame
        memcpy(pSrcAudioFrame->data[0],
            bits + (nbSentSamples * audioBytesPerSample * audioNumChannels),
            nbSamplesToSend * audioBytesPerSample * audioNumChannels);

        // convert into the stream sample format
        ret = swr_convert_frame(pSwrCtx, pEncAudioFrame, pSrcAudioFrame);
        if (ret < 0) {
            std::cerr << "Error converting frame: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
            return false;
        }

        // encode the frame
        pEncAudioFrame->pts = nextAudioPts;
        nextAudioPts += nbSamplesToSend;

        ret = avcodec_send_frame(pAudioCodecContext, pEncAudioFrame);
        if (ret < 0) {
            std::cerr << "Error sending frame: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
            return false;
        }

        ret = avcodec_receive_packet(pAudioCodecContext, &pkt);
        if (ret < 0) {
            //std::cerr << "Error receiving packet: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
            return false;
        }

        pkt.stream_index = pAudioStream->index;
        av_packet_rescale_ts(&pkt, pAudioCodecContext->time_base, pAudioStream->time_base);

        ret = av_write_frame(pOutputContext, &pkt);
        if (ret < 0) {
            std::cerr << "Error while writing audio frame: " << av_make_error_string(errorBuff, 128, ret) << std::endl;
            return false;
        }

        av_packet_unref(&pkt);

        nbSentSamples += nbSamplesToSend;
    }

    av_frame_free(&pSrcAudioFrame);
    av_frame_free(&pEncAudioFrame);

    return true;
}
