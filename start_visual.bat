@echo off

REM -------------------
REM Set local variables
REM -------------------

SET CMAKE_HOME=D:\Dev\cmake\cmake-3.12.3-win64-x64
SET VC="D:\Dev\Microsoft Visual Studio\2017\Community\Common7\IDE\devenv.exe"
SET FFMPEG_HOME=D:\Dev\libs\ffmpeg-4.0.2-win64-dev

REM -----------
REM Update PATH
REM -----------

SET PATH=%FFMPEG_HOME%\bin;%PATH%

REM -------------------
REM CMake
REM -------------------

%CMAKE_HOME%\bin\cmake . -G"Visual Studio 15 2017 Win64"

REM -------------------
REM Start Visual Studio
REM -------------------

start %VC% WinDesktopBroadcast.sln

@pause
